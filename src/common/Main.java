package common;

import com.esotericsoftware.minlog.Log;
import game.core.GameCore;
import org.lwjgl.LWJGLUtil;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import java.io.File;

public class Main {

    private static int width = 1024;
    private static int height = 600;

    private static GameCore gameCore;
    private static AppGameContainer gameContainer;
    private static String gameTitle = "Apocalytica";

    public static void main(String[] args) {
        System.setProperty(
                "org.lwjgl.librarypath",
                new File(
                        new File(System.getProperty("user.dir"), "native"),
                        LWJGLUtil.getPlatformName()
                ).getAbsolutePath()
        );
        System.setProperty("net.java.games.input.librarypath", System.getProperty("org.lwjgl.librarypath"));

        Log.set(Log.LEVEL_NONE);
        try {
            gameCore = new GameCore(gameTitle);
            gameContainer = new AppGameContainer(gameCore);
            gameContainer.setDisplayMode(width, height, false);
            gameContainer.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static AppGameContainer getGameContainer() {
        return gameContainer;
    }

    public static void setGameContainer(AppGameContainer gc) {
        gameContainer = gc;
    }

    public static GameCore getGameCore() {
        return gameCore;
    }

    public static String getGameTitle() {
        return gameTitle;
    }

    public static void setGameTitle(String gameTitle) {
        Main.gameTitle = gameTitle;
    }

}