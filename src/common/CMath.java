package common;

import game.world.environment.Map;


public class CMath {

    public static int getOposite(int x) {
        if (x < 0)
            return -1 * x;
        else if (x > 0)
            return -x;

        return x;
    }

    public static float getOposite(float x) {
        if (x < 0)
            return -1 * x;
        else if (x > 0)
            return -x;

        return x;
    }

    /**
     * * Method : getRelativePointFromWorld(float chunkX, float chunkY, float playerX, float playerY)
     * * Parameters :
     * * 		chunkX [float] : The chunkX position in chunkMap scale
     * * 		chunkY [float] : The chunkY position in chunkMap scale
     * * 		playerX [float] : The player X position in the world
     * * 		playerY [float] : The player Y position in the world
     * * Return : Return a 2D point of the player position in the chunk
     **/
    public static Point getRelativePointFromWorld(float chunkX, float chunkY, float playerX, float playerY) {
        float nX;
        float nY;

        nX = playerX - chunkX;
        nY = playerY - chunkY;

        Point point = new Point(nX, nY);

        return point;
    }

    public static int getCellIDFromChunk(float playerInChunkX, float playerInChunkY) {
        int cellID = -1;

        int x = (int) Math.ceil(playerInChunkX / Map.getTileWidth());
        int y = (int) Math.ceil(playerInChunkY / Map.getTileHeight());

        cellID = (x * y) + ((Map.getChunkWidth() - x) * (y - 1));

        return cellID;
    }

}
