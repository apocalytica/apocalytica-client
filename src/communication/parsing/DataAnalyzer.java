package communication.parsing;

import communication.GameServer;
import communication.GameSocketFactory;
import communication.SocketFactory;

public class DataAnalyzer {

    public DataAnalyzer() {

    }

    public static void parse(String datas) {
        switch (datas.charAt(0)) {
            case 'A': // Account
                AccountParser.parse(datas);

                break;

            case 'C': // Character
                CharacterParser.parse(datas);

                break;

            case 'E': // Engine
                EngineParser.parse(datas);

                break;

            case 'G': // Game
                GameParser.parse(datas);

                break;

            case 'T': // Transfert
                switch (datas.charAt(1)) {
                    case 'r': // ticket
                        String[] tDatas = datas.substring(2).split(";");
                        GameServer gs = SocketFactory.getGameServerList().get(Integer.parseInt(tDatas[2]));

                        SocketFactory.setAccountID(Integer.parseInt(tDatas[0]));
                        SocketFactory.setTicket(tDatas[1]);

                        new GameSocketFactory(gs.getIP(), gs.getPort());

                        break;

                    case 'i': // Identification
                        GameSocketFactory.sendPacket("Ti" + SocketFactory.getAccountID() + ";" + SocketFactory.getTicket());

                        break;
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
