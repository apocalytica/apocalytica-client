package communication.parsing;

import common.Main;
import communication.SocketFactory;
import game.core.controllers.LoginController;
import game.core.states.LoginState;

public class AccountParser {

    public AccountParser() {

    }

    public static void parse(String datas) {
        switch (datas.charAt(1)) {
            case 'L': // Login packet
                if (datas.substring(2).equals("ea")) // Error Account
                {
                    LoginController.createPopup("popup", "Account error !");
                } else if (datas.substring(2).equals("eb")) // Error Banned
                {
                    LoginController.createPopup("popup", "Account is banned !");
                } else if (datas.substring(2).equals("ep")) // Error Password
                {
                    LoginController.createPopup("popup", "Wrong password !");
                } else if (datas.substring(2).equals("ok")) // Can Connect
                {
                    LoginState.setIsLogged(true);
                    Main.getGameCore().enterState(2);
                    SocketFactory.sendPacket("GL");
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
