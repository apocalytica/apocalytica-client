package communication.parsing;

import communication.GameServer;
import communication.SocketFactory;
import de.lessvoid.nifty.controls.ListBox;
import game.core.states.ServerSelectionState;

public class GameParser {

    public GameParser() {

    }

    public static void parse(String datas) {
        switch (datas.charAt(1)) {
            case 'L': //List
                ServerSelectionState.getCurrentNifty().getScreen("start").findNiftyControl("serverList", ListBox.class).clear();

                int id;
                String name;
                String ip;
                int port;
                boolean online;

                String serverList = datas.substring(2);
                String servers[] = serverList.split("\\|");

                for (int i = 0; i < servers.length; i++) {
                    String[] infos = servers[i].split(";");

                    id = Integer.parseInt(infos[0]);
                    name = infos[1];
                    ip = infos[2];
                    port = Integer.parseInt(infos[3]);
                    online = ((Integer.parseInt(infos[5]) == 1) ? true : false);

                    SocketFactory.addGameServer(new GameServer(id, name, ip, port, online));

                    ServerSelectionState.getCurrentNifty().getScreen("start").findNiftyControl("serverList", ListBox.class).addItem(name + " (" + ((online) ? "Online" : "Offline") + ")");
                }

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
