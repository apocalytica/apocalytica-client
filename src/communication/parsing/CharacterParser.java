package communication.parsing;

import communication.GameSocketFactory;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.TextField;
import game.core.states.CharacterCreationState;
import game.core.states.CharacterSelectionState;
import game.world.entities.Player;

public class CharacterParser {

    public CharacterParser() {

    }

    public static void parse(String datas) {
        switch (datas.charAt(1)) {
            case 'L': // List
                CharacterSelectionState.getCurrentNifty().getScreen("start").findNiftyControl("characterList", ListBox.class).clear();

                long id;
                String name;
                int level;

                String serverList = datas.substring(2);
                String servers[] = serverList.split(",");

                for (int i = 0; i < servers.length; i++) {
                    String[] infos = servers[i].split(";");

                    id = Long.parseLong(infos[0]);
                    name = infos[1];
                    level = Integer.parseInt(infos[2]);


                    GameSocketFactory.addCharacter(new Player(id, name, level));

                    CharacterSelectionState.getCurrentNifty().getScreen("start").findNiftyControl("characterList", ListBox.class).addItem(name + " (" + level + ")");
                }

                break;

            case 'R': // Random character name
                CharacterCreationState.getCurrentNifty().getScreen("start").findNiftyControl("nameInput", TextField.class).setText(datas.substring(3));

                break;

            default: // Not defined packets
                System.out.println("Packet is unkwnown");

                break;
        }
    }

}
