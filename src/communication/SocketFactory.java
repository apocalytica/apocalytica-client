package communication;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import common.Main;
import communication.parsing.DataAnalyzer;
import de.lessvoid.nifty.controls.TextField;
import game.core.controllers.LoginController;
import game.core.states.LoginState;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class SocketFactory {

    private static Client client;
    private static HashMap<Integer, GameServer> gsList = new HashMap<Integer, GameServer>();
    private static int accountID;
    private static String ticket;

    public SocketFactory() {
        try {

            client = new Client();

            client.addListener(new Listener() {
                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof String) {
                        String packet = (String) object;
                        System.out.println("Recv << " + packet);
                        DataAnalyzer.parse(packet);
                    }
                }

                @Override
                public void connected(Connection connection) {
                    connect();
                }

                @Override
                public void disconnected(Connection connection) {
                    System.out.println("Disconnected !");
                    if (LoginState.getIsLogged()) {
                        Main.getGameCore().enterState(1);
                    }

                    if (!LoginController.getPopupIsOpened() && !LoginState.getIsLogged())
                        LoginController.createPopup("popup", "Disconnected from the server !");

                    LoginState.setIsLogged(false);
                }
            });

            client.start();
            client.connect(5000, "127.0.0.1", 9902, 9903);

        } catch (IOException e) {
            LoginController.createPopup("popup", "Unable to connect to the server !");
        }
    }

    public static Client getClient() {
        return client;
    }

    public static String getTicket() {
        return ticket;
    }

    public static void setTicket(String pTicket) {
        ticket = pTicket;
    }

    public static HashMap<Integer, GameServer> getGameServerList() {
        return gsList;
    }

    public static void addGameServer(GameServer gs) {
        gsList.put(gs.getID(), gs);
    }

    public static void sendPacket(String packet) {
        if (client.isConnected()) {
            System.out.println("Send (TCP) >> " + packet);
            client.sendTCP(packet);
        }
    }

    public static void sendFasterPacket(String packet) {
        if (client.isConnected()) {
            System.out.println("Send (UDP) >> " + packet);
            client.sendUDP(packet);
        }
    }

    public static void connect() {
        String password = LoginController.getNifty().getScreen("start").findNiftyControl("passInput", (TextField.class)).getText();
        String account = LoginController.getNifty().getScreen("start").findNiftyControl("userInput", TextField.class).getText();
        String salt = account + password;
        byte[] hashedPass = null;

        try {
            byte[] pass2 = MessageDigest.getInstance("MD5").digest(salt.getBytes());
            hashedPass = MessageDigest.getInstance("SHA-1").digest(pass2);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        sendPacket("AL" + account + ";" + new String(hashedPass));
    }

    public static void reconnect() {
        try {
            client.reconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void disconnect() {
        client.close();
    }

    public static int getAccountID() {
        return accountID;
    }

    public static void setAccountID(int id) {
        accountID = id;
    }

}
