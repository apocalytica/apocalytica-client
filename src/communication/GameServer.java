package communication;

import java.util.Iterator;

public class GameServer {

    private int ID;
    private String name;
    private String IP;
    private int port;
    private boolean isOnline;

    public GameServer(int pID, String pName, String pIP, int port, boolean pIsOnline) {
        this.ID = pID;
        this.name = pName;
        this.IP = pIP;
        this.port = port;
        this.isOnline = pIsOnline;
    }

    public static GameServer getGameServerByName(String gsName) {
        Iterator<GameServer> vIter = SocketFactory.getGameServerList().values().iterator();

        while (vIter.hasNext()) {
            GameServer value = vIter.next();

            if (value.getName().equalsIgnoreCase(gsName)) {
                return value;
            }
        }

        return null;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String iP) {
        IP = iP;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

}
