package communication;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import common.Main;
import communication.parsing.DataAnalyzer;
import game.core.controllers.LoginController;
import game.core.states.LoginState;
import game.world.entities.Player;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class GameSocketFactory {

    private static Client client;
    private static HashMap<Long, Player> characterList = new HashMap<Long, Player>();

    public GameSocketFactory(String gsIP, int gsPort) {
        try {

            client = new Client();

            client.addListener(new Listener() {
                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof String) {
                        String packet = (String) object;
                        System.out.println("Recv << " + packet);
                        DataAnalyzer.parse(packet);
                    }
                }

                @Override
                public void connected(Connection connection) {
                    Main.getGameCore().enterState(4);
                }

                @Override
                public void disconnected(Connection connection) {
                    System.out.println("Disconnected !");
                    if (LoginState.getIsLogged()) {
                        Main.getGameCore().enterState(1);
                    }

                    if (!LoginController.getPopupIsOpened() && !LoginState.getIsLogged())
                        LoginController.createPopup("popup", "Disconnected from the server !");

                    LoginState.setIsLogged(false);
                }
            });

            client.start();
            client.connect(5000, gsIP, gsPort);
            System.out.println("Connected to GameServer !");

        } catch (IOException e) {
            LoginController.createPopup("popup", "Unable to connect to the server !");
        }
    }

    public static Client getClient() {
        return client;
    }

    public static void sendPacket(String packet) {
        if (client.isConnected()) {
            System.out.println("Send (TCP) >> " + packet);
            client.sendTCP(packet);
        }
    }

    public static void sendFasterPacket(String packet) {
        if (client.isConnected()) {
            System.out.println("Send (UDP) >> " + packet);
            client.sendUDP(packet);
        }
    }

    public static void disconnect() {
        client.close();
    }

    public static void addCharacter(Player player) {
        characterList.put(player.getGUID(), player);
    }

    public static HashMap<Long, Player> getCharacters() {
        return characterList;
    }

    public static Player getCharacterByName(String name) {
        Iterator<Player> vIter = characterList.values().iterator();

        while (vIter.hasNext()) {
            Player value = vIter.next();

            if (value.getName().equals(name)) {
                return value;
            }
        }

        return null;
    }

}
