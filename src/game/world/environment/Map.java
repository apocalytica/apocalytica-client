package game.world.environment;

import common.CMath;
import game.core.states.GameCoreState;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Graphics;

import java.util.HashMap;

public class Map {

    private static int chunkWidth = 0;
    private static int chunkHeight = 0;
    private static int tileWidth = 0;
    private static int tileHeight = 0;
    private static HashMap<Integer, Map> mapList = new HashMap<Integer, Map>();
    private int mapID;
    private HashMap<Long, Chunk> chunkList = new HashMap<Long, Chunk>();
    private HashMap<String, Long> chunkMap = new HashMap<String, Long>();

    // JBox2d
    private World attachedWorld;

    private long refChunkID;
    private long _TLCID;
    private long _TCCID;
    private long _TRCID;
    private long _MLCID;
    private long _MRCID;
    private long _BLCID;
    private long _BCCID;
    private long _BRCID;

    public Map(int mapID) {
        this.mapID = mapID;
        mapList.put(mapID, this);

        attachedWorld = new World(new Vec2(0, 0), true);
    }

    public static HashMap<Integer, Map> getMapList() {
        return mapList;
    }

    public static void addMapList(Map map) {
        Map.mapList.put(map.getMapID(), map);
    }

    public static int getChunkWidth() {
        return chunkWidth;
    }

    public static void setChunkWidth(int chunkWidth) {
        Map.chunkWidth = chunkWidth;
    }

    public static int getChunkHeight() {
        return chunkHeight;
    }

    public static void setChunkHeight(int chunkHeight) {
        Map.chunkHeight = chunkHeight;
    }

    public static int getTileWidth() {
        return tileWidth;
    }

    public static void setTileWidth(int tileWidth) {
        Map.tileWidth = tileWidth;
    }

    public static int getTileHeight() {
        return tileHeight;
    }

    public static void setTileHeight(int tileHeight) {
        Map.tileHeight = tileHeight;
    }

    public void update() {
        attachedWorld.step(1.0f / 60.0f, 6, 2);
        attachedWorld.clearForces();
        refChunkID = GameCoreState.getChunkID();

        if (refChunkID != -1) {
            // Get chunk around refChunk
            Chunk refChunk = getChunkList().get(new Long(refChunkID));
            int chunkX = refChunk.getX();
            int chunkY = refChunk.getY();

            // Top Left Chunk
            Object value = chunkMap.get((chunkX - 1) + "/" + (chunkY - 1));
            _TLCID = ((value == null) ? -1 : (Long) value);

            // Top Center Chunk
            Object value2 = chunkMap.get(chunkX + "/" + (chunkY - 1));
            _TCCID = ((value2 == null) ? -1 : (Long) value2);

            // Top Right Chunk
            Object value3 = chunkMap.get((chunkX + 1) + "/" + (chunkY - 1));
            _TRCID = ((value3 == null) ? -1 : (Long) value3);

            // Middle Left Chunk
            Object value4 = chunkMap.get((chunkX - 1) + "/" + chunkY);
            _MLCID = ((value4 == null) ? -1 : (Long) value4);

            // Middle Right Chunk
            Object value5 = chunkMap.get((chunkX + 1) + "/" + chunkY);
            _MRCID = ((value5 == null) ? -1 : (Long) value5);

            // Bottom Left Chunk
            Object value6 = chunkMap.get((chunkX - 1) + "/" + (chunkY + 1));
            _BLCID = ((value6 == null) ? -1 : (Long) value6);

            // Bottom Center Chunk
            Object value7 = chunkMap.get(chunkX + "/" + (chunkY + 1));
            _BCCID = ((value7 == null) ? -1 : (Long) value7);

            // Bottom Right Chunk
            Object value8 = chunkMap.get((chunkX + 1) + "/" + (chunkY + 1));
            _BRCID = ((value8 == null) ? -1 : (Long) value8);
        }
    }

    public void render(Graphics g) {
        Chunk chunk1 = getChunkList().get(new Long(_TLCID));
        Chunk chunk2 = getChunkList().get(new Long(_TCCID));
        Chunk chunk3 = getChunkList().get(new Long(_TRCID));
        Chunk chunk4 = getChunkList().get(new Long(_MLCID));
        Chunk chunk5 = getChunkList().get(new Long(refChunkID));
        Chunk chunk6 = getChunkList().get(new Long(_MRCID));
        Chunk chunk7 = getChunkList().get(new Long(_BLCID));
        Chunk chunk8 = getChunkList().get(new Long(_BCCID));
        Chunk chunk9 = getChunkList().get(new Long(_BRCID));

        if (refChunkID != -1) {
            if (chunk1 != null)
                chunk1.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk1.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk1.getY() * (chunkHeight * tileHeight)));

            if (chunk2 != null)
                chunk2.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk2.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk2.getY() * (chunkHeight * tileHeight)));

            if (chunk3 != null)
                chunk3.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk3.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk3.getY() * (chunkHeight * tileHeight)));

            if (chunk4 != null)
                chunk4.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk4.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk4.getY() * (chunkHeight * tileHeight)));

            if (chunk5 != null)
                chunk5.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk5.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk5.getY() * (chunkHeight * tileHeight)));

            if (chunk6 != null)
                chunk6.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk6.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk6.getY() * (chunkHeight * tileHeight)));

            if (chunk7 != null)
                chunk7.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk7.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk7.getY() * (chunkHeight * tileHeight)));

            if (chunk8 != null)
                chunk8.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk8.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk8.getY() * (chunkHeight * tileHeight)));

            if (chunk9 != null)
                chunk9.getChunkMap().render(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX() + chunk9.getX() * (chunkWidth * tileWidth)),
                        Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY() + chunk9.getY() * (chunkHeight * tileHeight)));
        }
    }

    public HashMap<Long, Chunk> getChunkList() {
        return chunkList;
    }

    public void addChunkList(Chunk chunk) {
        if (chunkWidth == 0 || chunkHeight == 0) {
            chunkWidth = chunk.getChunkMap().getWidth();
            chunkHeight = chunk.getChunkMap().getHeight();
        }
        if (tileWidth == 0 || tileHeight == 0) {
            tileWidth = chunk.getChunkMap().getTileWidth();
            tileHeight = chunk.getChunkMap().getTileHeight();
        }
        this.chunkList.put(chunk.getChunkID(), chunk);
        this.addChunkMap(chunk.getX() + "/" + chunk.getY(), chunk.getChunkID());
    }

    public HashMap<String, Long> getChunkMap() {
        return chunkMap;
    }

    public void addChunkMap(String position, long chunkID) {
        this.chunkMap.put(position, chunkID);
    }

    public int getMapID() {
        return mapID;
    }

    public void setMapID(int mapID) {
        this.mapID = mapID;
    }

    public void goToChunk(long chunkID, float x, float y) {
        GameCoreState.setChunkID(chunkID);
        GameCoreState.setChunkX(x);
        GameCoreState.setChunkY(y);
    }

    public long getChunkIDFromPosition(float x, float y, int mapWidth, int mapHeight) {
        // X AXIS
        int xID = CMath.getOposite((int) Math.floor((x / (mapWidth * 32))) + (((x % (mapWidth * 32)) > 0) ? 1 : 0)) - 1;

        // Y AXIS
        int yID = CMath.getOposite((int) Math.floor((y / (mapHeight * 32))) + (((y % (mapHeight * 32)) > 0) ? 1 : 0)) - 1;

        if (chunkMap.get(xID + "/" + yID) != null) {
            return chunkMap.get(xID + "/" + yID);
        }

        return -1;
    }

    public World getAttachedWorld() {
        return attachedWorld;
    }

    public void setAttachedWorld(World attachedWorld) {
        this.attachedWorld = attachedWorld;
    }

}