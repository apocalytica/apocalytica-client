package game.world.environment;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

public class Chunk {

    private TiledMap chunk;
    private long chunkID;
    private int x;
    private int y;

    public Chunk(String tiledPath, String resPath, Map map, long chunkID, int x, int y) throws SlickException {
        chunk = new TiledMap(tiledPath, resPath);

        this.chunkID = chunkID;
        this.x = x;
        this.y = y;

        map.addChunkList(this);
    }

    public TiledMap getChunkMap() {
        return chunk;
    }

    public void setChunkMap(TiledMap map) {
        this.chunk = map;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public long getChunkID() {
        return chunkID;
    }

    public void setChunkID(long chunkID) {
        this.chunkID = chunkID;
    }

}
