package game.world.entities;

public abstract class Entity {

    public abstract long getGUID();

    public abstract void setGUID(long pGUD);

    public abstract String getName();

    public abstract void setName(String pName);

    public abstract int getMapID();

    public abstract void setMapID(int mapID);

    public abstract long getChunkID();

    public abstract void setChunkID(long chunkID);

    public abstract float getX();

    public abstract void setX(float pX);

    public abstract float getY();

    public abstract void setY(float pY);

}
