package game.world.entities;

import game.world.environment.Map;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.GameContainer;


public class Player extends Entity {

    private long GUID;
    private String name;
    private int mapID;
    private long chunkID;
    private float x;
    private float y;
    private int level;
    private int life;
    private int mana;
    private long xp;
    private short rights;

    private BodyDef bodyDef;
    private Body dynamicBody;
    private PolygonShape dynamicBox;
    private FixtureDef dynamicFixDef;

    private BodyDef bodyDef2;
    private Body dynamicBody2;
    private PolygonShape dynamicBox2;
    private FixtureDef dynamicFixDef2;

    private float screenX;
    private float screenY;

    public Player(Long id, String pName, int pLevel) {
        this.GUID = id;
        this.name = pName;
        this.level = pLevel;
    }

    @Override
    public long getGUID() {
        return GUID;
    }

    @Override
    public void setGUID(long pGUID) {
        this.GUID = pGUID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String pName) {
        this.name = pName;
    }

    @Override
    public float getX() {
        return x;
    }

    @Override
    public void setX(float pX) {
        this.x = pX;
    }

    @Override
    public float getY() {
        return y;
    }

    @Override
    public void setY(float pY) {
        this.y = pY;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public long getXp() {
        return xp;
    }

    public void setXp(long xp) {
        this.xp = xp;
    }

    public short getRights() {
        return rights;
    }

    public void setRights(short rights) {
        this.rights = rights;
    }

    @Override
    public int getMapID() {
        return mapID;
    }

    @Override
    public void setMapID(int mapID) {
        this.mapID = mapID;
    }

    public float getScreenX() {
        return screenX;
    }

    public void setScreenX(float screenX) {
        this.screenX = screenX;
    }

    public float getScreenY() {
        return screenY;
    }

    public void setScreenY(float screenY) {
        this.screenY = screenY;
    }

    @Override
    public long getChunkID() {
        return chunkID;
    }

    @Override
    public void setChunkID(long chunkID) {
        this.chunkID = chunkID;
    }

    public void createPhysic() {
        bodyDef = new BodyDef();
        bodyDef.type = BodyType.DYNAMIC;
        bodyDef.position.x = x;
        bodyDef.position.y = y;

        dynamicBody = Map.getMapList().get(mapID).getAttachedWorld().createBody(bodyDef);
        dynamicBox = new PolygonShape();
        dynamicBox.setAsBox(10, 10);
        dynamicFixDef = new FixtureDef();
        dynamicFixDef.shape = dynamicBox;
        dynamicFixDef.density = 1.0f;
        dynamicFixDef.friction = 0.3f;
        dynamicBody.createFixture(dynamicFixDef);

        bodyDef2 = new BodyDef();
        bodyDef2.type = BodyType.STATIC;
        bodyDef2.position.x = 0;
        bodyDef2.position.y = 0;
        dynamicBody2 = Map.getMapList().get(mapID).getAttachedWorld().createBody(bodyDef2);
        dynamicBox2 = new PolygonShape();
        dynamicBox2.setAsBox(20, 20);
        dynamicFixDef2 = new FixtureDef();
        dynamicFixDef2.shape = dynamicBox2;
        dynamicFixDef2.density = 1.0f;
        dynamicFixDef2.friction = 0.3f;
        dynamicBody2.createFixture(dynamicFixDef2);
    }

    public void update() {
        dynamicBody.setTransform(new Vec2(0, 0), 0);

        dynamicBody2.setTransform(new Vec2(10, 10), 0);
    }

    public void render(GameContainer gc) {
        gc.getGraphics().fillOval(gc.getWidth() / 2, gc.getHeight() / 2, 5, 5);
    }

    public BodyDef getBodyDef() {
        return bodyDef;
    }
}
