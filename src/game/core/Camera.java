package game.core;

import game.world.entities.Player;
import org.newdawn.slick.GameContainer;


public class Camera {

    private float x = 0;
    private float y = 0;
    private float zoom = 1.0f;
    private Object target;

    public Camera() {

    }

    public Camera(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void render(GameContainer gc) {
        // Center the camera on the node
        if (target instanceof Player) {
            Player _target = (Player) target;

            this.x = _target.getX() - gc.getWidth() / 2;
            this.y = _target.getY() - gc.getHeight() / 2;
        }
    }

    public void drawTarget(GameContainer gc) {
        gc.getGraphics().fillOval(gc.getWidth() / 2, gc.getHeight() / 2, 5, 5);
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

}