package game.core;

import game.core.states.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameCore extends StateBasedGame {

    public GameCore(String title) {
        super(title);
    }

    @Override
    public void initStatesList(GameContainer gc) throws SlickException {
        addState(new LoginState());
        addState(new ServerSelectionState());
        addState(new CharacterCreationState());
        addState(new CharacterSelectionState());
        addState(new GameCoreState());

        enterState(5);
    }

}