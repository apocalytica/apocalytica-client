package game.core.controllers;

import communication.SocketFactory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.ButtonClickedEvent;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class LoginController implements ScreenController {

    private static Screen screen;
    private static Nifty nifty;
    private static Element popup;
    private static boolean popupIsOpened = false;

    public static void createPopup(String id, String message) {
        if (popup == null) {
            popup = nifty.createPopup(id);
        }

        popup.findElementByName("text").getRenderer(TextRenderer.class).setText(message);
        nifty.showPopup(screen, popup.getId(), null);
        popupIsOpened = true;
    }

    public static Element getPopup() {
        return popup;
    }

    public static Nifty getNifty() {
        return nifty;
    }

    public static Screen getScreen() {
        return screen;
    }

    public static boolean getPopupIsOpened() {
        return popupIsOpened;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        LoginController.nifty = nifty;
        LoginController.screen = screen;
    }

    @Override
    public void onEndScreen() {

    }

    @Override
    public void onStartScreen() {

    }

    @NiftyEventSubscriber(id = "loginBut")
    public void onLoginClick(final String id, final ButtonClickedEvent event) {
        if (!screen.findNiftyControl("userInput", TextField.class).getText().isEmpty() && !screen.findNiftyControl("passInput", TextField.class).getText().isEmpty()) {
            new SocketFactory();
        }
    }

    @NiftyEventSubscriber(id = "closePopup")
    public void onClosePopupClick(final String id, final ButtonClickedEvent event) {
        popupIsOpened = false;
        nifty.closePopup(popup.getId());
    }

}
