package game.core.controllers;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class GameCoreController implements ScreenController {

    private static Nifty nifty;
    private static Screen screen;

    public static Nifty getNifty() {
        return nifty;
    }

    public static Screen getScreen() {
        return screen;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        GameCoreController.nifty = nifty;
        GameCoreController.screen = screen;
    }

    @Override
    public void onEndScreen() {

    }

    @Override
    public void onStartScreen() {

    }

}
