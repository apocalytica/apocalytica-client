package game.core.controllers;

import communication.GameServer;
import communication.SocketFactory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.ButtonClickedEvent;
import de.lessvoid.nifty.controls.ListBoxSelectionChangedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class ServerSelectionController implements ScreenController {

    private static Nifty nifty;
    private static Screen screen;

    private String serverName;
    private int serverState;

    public static Nifty getNifty() {
        return nifty;
    }

    public static Screen getScreen() {
        return screen;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        ServerSelectionController.nifty = nifty;
        ServerSelectionController.screen = screen;
    }

    @Override
    public void onEndScreen() {

    }

    @Override
    public void onStartScreen() {
        nifty.getScreen("start").findNiftyControl("selectButton", Button.class).disable();
    }

    @NiftyEventSubscriber(id = "backButton")
    public void onBackClick(final String id, final ButtonClickedEvent event) {
        SocketFactory.disconnect();
    }

    @NiftyEventSubscriber(id = "serverList")
    public void onListBoxSelectionChanged(final String id, final ListBoxSelectionChangedEvent event) {
        String[] sel = event.getSelection().get(0).toString().split(" ");

        serverName = sel[0];
        if (sel[1].substring(1, sel[1].length() - 1).equals("Online")) {
            serverState = 1;
        } else {
            serverState = 0;
        }

        if (serverState == 1) {
            nifty.getScreen("start").findNiftyControl("selectButton", Button.class).enable();
        } else {
            nifty.getScreen("start").findNiftyControl("selectButton", Button.class).disable();
        }
    }

    @NiftyEventSubscriber(id = "selectButton")
    public void onSelectClick(final String id, final ButtonClickedEvent event) {
        GameServer gs = GameServer.getGameServerByName(serverName);
        int gsID = gs.getID();
        SocketFactory.sendPacket("GS" + gsID);
    }

}
