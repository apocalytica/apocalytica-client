package game.core.controllers;

import common.Main;
import communication.GameSocketFactory;
import communication.SocketFactory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.ButtonClickedEvent;
import de.lessvoid.nifty.controls.ListBoxSelectionChangedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import game.world.entities.Player;

public class CharacterSelectionController implements ScreenController {

    private static Nifty nifty;
    private static Screen screen;
    private String characterName;

    public static Nifty getNifty() {
        return nifty;
    }

    public static Screen getScreen() {
        return screen;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        CharacterSelectionController.nifty = nifty;
        CharacterSelectionController.screen = screen;
    }

    @Override
    public void onEndScreen() {

    }

    @Override
    public void onStartScreen() {
        nifty.getScreen("start").findNiftyControl("selectButton", Button.class).disable();
    }

    @NiftyEventSubscriber(id = "characterList")
    public void onCharacterSelectionChanged(final String id, final ListBoxSelectionChangedEvent event) {
        String[] sel = event.getSelection().get(0).toString().split(" ");

        characterName = sel[0];
    }

    @NiftyEventSubscriber(id = "CSBackButton")
    public void onBackClick(final String id, final ButtonClickedEvent event) {
        Main.getGameCore().enterState(2);
        SocketFactory.sendPacket("GL");
    }

    @NiftyEventSubscriber(id = "CSCreateButton")
    public void onCreateClick(final String id, final ButtonClickedEvent event) {
        Main.getGameCore().enterState(3);
    }

    @NiftyEventSubscriber(id = "CSSelectButton")
    public void onSelectClick(final String id, final ButtonClickedEvent event) {
        Player character = GameSocketFactory.getCharacterByName(characterName);
        long charID = character.getGUID();
        GameSocketFactory.sendPacket("CS" + charID);
        GameSocketFactory.getCharacters().clear();
    }

}
