package game.core.controllers;

import communication.GameSocketFactory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.ButtonClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class CharacterCreationController implements ScreenController {

    private static Nifty nifty;
    private static Screen screen;

    public static Nifty getNifty() {
        return nifty;
    }

    public static Screen getScreen() {
        return screen;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        CharacterCreationController.nifty = nifty;
        CharacterCreationController.screen = screen;
    }

    @Override
    public void onEndScreen() {

    }

    @Override
    public void onStartScreen() {

    }

    @NiftyEventSubscriber(id = "randName")
    public void onRandNameClick(final String id, final ButtonClickedEvent event) {
        GameSocketFactory.sendPacket("CR");
    }

}
