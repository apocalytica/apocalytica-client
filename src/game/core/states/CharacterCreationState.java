package game.core.states;

import common.Main;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.textfield.builder.TextFieldBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.slick2d.NiftyBasicGameState;
import de.lessvoid.nifty.tools.Color;
import game.core.controllers.CharacterCreationController;
import org.newdawn.slick.state.StateBasedGame;

public class CharacterCreationState extends NiftyBasicGameState {

    private static Nifty nifty;

    public static Nifty getCurrentNifty() {
        return nifty;
    }

    @Override
    protected void prepareNifty(Nifty nifty, StateBasedGame sb) {
        CharacterCreationState.nifty = nifty;

        java.util.logging.Logger.getAnonymousLogger().getParent().setLevel(java.util.logging.Level.SEVERE);
        java.util.logging.Logger.getLogger("de.lessvoid.nifty.*").setLevel(java.util.logging.Level.SEVERE);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        Screen CCScreen = new ScreenBuilder("start") {{
            controller(new CharacterCreationController());

            layer(new LayerBuilder("CCLayer") {{
                backgroundColor("#003f");
                childLayoutCenter();
                width("*");
                height("*");

                panel(new PanelBuilder("CCMainPanel") {{
                    childLayoutAbsolute();
                    width(percentage(100));
                    height(percentage(100));
                    backgroundColor(Color.BLACK);

                    control(new LabelBuilder("nameLabel") {{
                        text("Name : ");
                        width("200px");
                        height("20px");
                        x((Main.getWidth() / 2) - 100 + "px");
                        y(Main.getHeight() - 120 + "px");
                    }});

                    control(new TextFieldBuilder("nameInput") {{
                        maxLength(20);
                        width("200px");
                        height("20px");
                        x((Main.getWidth() / 2) - 100 + "px");
                        y(Main.getHeight() - 100 + "px");
                    }});

                    control(new ButtonBuilder("randName") {{
                        text("@");
                        width("20px");
                        height("20px");
                        x((Main.getWidth() / 2) + 105 + "px");
                        y(Main.getHeight() - 100 + "px");
                    }});
                }});
            }});
        }}.build(nifty);

        nifty.addScreen("start", CCScreen);
    }

    @Override
    public int getID() {
        return 3;
    }

}
