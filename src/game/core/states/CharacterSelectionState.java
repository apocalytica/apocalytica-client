package game.core.states;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.listbox.builder.ListBoxBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.slick2d.NiftyBasicGameState;
import de.lessvoid.nifty.tools.Color;
import game.core.controllers.CharacterSelectionController;
import org.newdawn.slick.state.StateBasedGame;

public class CharacterSelectionState extends NiftyBasicGameState {

    private static Nifty nifty;

    public static Nifty getCurrentNifty() {
        return nifty;
    }

    @Override
    protected void prepareNifty(Nifty nifty, StateBasedGame sb) {
        CharacterSelectionState.nifty = nifty;

        java.util.logging.Logger.getAnonymousLogger().getParent().setLevel(java.util.logging.Level.SEVERE);
        java.util.logging.Logger.getLogger("de.lessvoid.nifty.*").setLevel(java.util.logging.Level.SEVERE);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        Screen CSScreen = new ScreenBuilder("start") {{
            controller(new CharacterSelectionController());

            layer(new LayerBuilder("CSLayer") {{
                backgroundColor("#003f");
                childLayoutCenter();
                width("*");
                height("*");

                panel(new PanelBuilder("CSMainPanel") {{
                    childLayoutAbsolute();
                    width("500px");
                    height("280px");
                    backgroundColor(Color.WHITE);

                    control(new ListBoxBuilder("characterList") {{
                        x("10");
                        y("10");
                        width("480px");
                        height("250px");
                        displayItems(10);
                        hideHorizontalScrollbar();
                        optionalVerticalScrollbar();
                    }});

                    control(new ButtonBuilder("CSBackButton", "Back") {{
                        x("80");
                        y("250px");
                        width("100px");
                        height("20px");
                    }});

                    control(new ButtonBuilder("CSCreateButton", "Create") {{
                        x("200");
                        y("250px");
                        width("100px");
                        height("20px");
                    }});

                    control(new ButtonBuilder("CSSelectButton", "Select") {{
                        x("320");
                        y("250px");
                        width("100px");
                        height("20px");
                    }});

                }});
            }});
        }}.build(nifty);

        nifty.addScreen("start", CSScreen);
    }

    @Override
    public int getID() {
        return 4;
    }

}
