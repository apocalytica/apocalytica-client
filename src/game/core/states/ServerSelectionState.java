package game.core.states;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.listbox.builder.ListBoxBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.slick2d.NiftyBasicGameState;
import de.lessvoid.nifty.tools.Color;
import game.core.controllers.ServerSelectionController;
import org.newdawn.slick.state.StateBasedGame;

public class ServerSelectionState extends NiftyBasicGameState {

    private static Nifty nifty;

    public static Nifty getCurrentNifty() {
        return nifty;
    }

    @Override
    protected void prepareNifty(Nifty nifty, StateBasedGame sb) {
        ServerSelectionState.nifty = nifty;

        //java.util.logging.Logger.getAnonymousLogger().getParent().setLevel(java.util.logging.Level.SEVERE);
        //java.util.logging.Logger.getLogger("de.lessvoid.nifty.*").setLevel(java.util.logging.Level.SEVERE);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        Screen SSScreen = new ScreenBuilder("start") {{
            controller(new ServerSelectionController());

            layer(new LayerBuilder("SSLayer") {{
                backgroundColor("#003f");
                childLayoutCenter();
                width("*");
                height("*");

                panel(new PanelBuilder("SSMainPanel") {{
                    childLayoutAbsolute();
                    width("500px");
                    height("280px");
                    backgroundColor(Color.WHITE);

                    control(new ListBoxBuilder("serverList") {{
                        x("10");
                        y("10");
                        width("480px");
                        height("250px");
                        displayItems(10);
                        hideHorizontalScrollbar();
                        optionalVerticalScrollbar();
                    }});

                    control(new ButtonBuilder("backButton", "Back") {{
                        x("100");
                        y("250px");
                        width("100px");
                        height("20px");
                    }});

                    control(new ButtonBuilder("selectButton", "Select") {{
                        x("250");
                        y("250px");
                        width("100px");
                        height("20px");
                    }});

                }});
            }});
        }}.build(nifty);

        nifty.addScreen("start", SSScreen);
    }

    @Override
    public int getID() {
        return 2;
    }

}
