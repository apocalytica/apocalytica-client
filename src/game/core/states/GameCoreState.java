package game.core.states;

import common.CMath;
import common.Main;
import common.Point;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.slick2d.NiftyOverlayBasicGameState;
import game.core.Camera;
import game.core.Slick2dDebugDraw;
import game.core.controllers.GameCoreController;
import game.world.entities.Player;
import game.world.environment.Chunk;
import game.world.environment.Map;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameCoreState extends NiftyOverlayBasicGameState {

    private static Map map;
    private static Camera camera;
    private static long chunkID = 1;
    //private static float originWorldX = 157495f;
    //private static float originWorldY = 157495f;
    private static float originWorldX = 100f;
    private static float originWorldY = 100f;
    private static float chunkX = 0f;
    private static float chunkY = 0f;
    private static int cellID;
    private static Player followedPlayer;
    private float movementSpeed = 0.5f;
    private boolean move = false;

    private float xMovement;
    private float yMovement;

    private boolean leftMovement = false;
    private boolean rightMovement = false;
    private boolean upMovement = false;
    private boolean downMovement = false;

    private boolean leftCollision = false;
    private boolean rightCollision = false;
    private boolean upCollision = false;
    private boolean downCollision = false;

    public GameCoreState() {

    }

    public static Map getMap() {
        return map;
    }

    public static void setMap(Map map) {
        GameCoreState.map = map;
    }

    public static float getChunkX() {
        return chunkX;
    }

    public static void setChunkX(float x) {
        GameCoreState.chunkX = x;
    }

    public static float getChunkY() {
        return chunkY;
    }

    public static void setChunkY(float y) {
        GameCoreState.chunkY = y;
    }

    public static Player getFollowedPlayer() {
        return followedPlayer;
    }

    public static void setFollowedPlayer(Player followedPlayer) {
        GameCoreState.followedPlayer = followedPlayer;
    }

    public static long getChunkID() {
        return chunkID;
    }

    public static void setChunkID(long chunkID) {
        GameCoreState.chunkID = chunkID;
    }

    public static long getCellID() {
        return cellID;
    }

    public static Camera getCamera() {
        return camera;
    }

    public static void setCamera(Camera camera) {
        GameCoreState.camera = camera;
    }

    public static float getOriginWorldX() {
        return originWorldX;
    }

    public static void setOriginWorldX(float originWorldX) {
        GameCoreState.originWorldX = originWorldX;
    }

    public static float getOriginWorldY() {
        return originWorldY;
    }

    public static void setOriginWorldY(float originWorldY) {
        GameCoreState.originWorldY = originWorldY;
    }

    @Override
    protected void enterState(GameContainer gc, StateBasedGame sb) throws SlickException {

    }

    @Override
    protected void initGameAndGUI(GameContainer gc, StateBasedGame sb) throws SlickException {
        System.out.println("Init GUI ...");
        initNifty(gc, sb);
        System.out.println("Init GUI finished !");

        System.out.println("Init game ...");
        System.out.println("Create Map");
        map = new Map(1);

        System.out.println("Load and add its chunks");
        map.addChunkList(new Chunk("game/res/small1.tmx", "game/res", map, 1, 0, 0));
        map.addChunkList(new Chunk("game/res/small2.tmx", "game/res", map, 2, 1, 0));
        map.addChunkList(new Chunk("game/res/small3.tmx", "game/res", map, 3, 2, 0));

        map.addChunkList(new Chunk("game/res/small4.tmx", "game/res", map, 4, 0, 1));
        map.addChunkList(new Chunk("game/res/small5.tmx", "game/res", map, 5, 1, 1));
        map.addChunkList(new Chunk("game/res/small6.tmx", "game/res", map, 6, 2, 1));

        map.addChunkList(new Chunk("game/res/small7.tmx", "game/res", map, 7, 0, 2));
        map.addChunkList(new Chunk("game/res/small8.tmx", "game/res", map, 8, 1, 2));
        map.addChunkList(new Chunk("game/res/small9.tmx", "game/res", map, 9, 2, 2));

        System.out.println("Create and set player");
        followedPlayer = new Player(1L, "Kyu", 25);
        followedPlayer.setScreenX(Main.getWidth() / 2);
        followedPlayer.setScreenY(Main.getHeight() / 2);
        followedPlayer.setMapID(map.getMapID());
        followedPlayer.setChunkID(chunkID);
        // World position
        followedPlayer.setX(originWorldX + 100);
        followedPlayer.setY(originWorldY + 100);

        // Player physic
        System.out.println("Set player physic");
        Slick2dDebugDraw sDD = new Slick2dDebugDraw(gc.getGraphics(), gc); // arg0 is the GameContainer in this case, I put this code in my init method
        sDD.setFlags(0x0001); //Setting the debug draw flags,
		/*
		Here are the flags available,
		public static final int e_shapeBit                              = 0x0001; ///< draw shapes
		        public static final int e_jointBit                              = 0x0002; ///< draw joint connections
		        public static final int e_coreShapeBit                  = 0x0004; ///< draw core (TOI) shapes
		        public static final int e_aabbBit                               = 0x0008; ///< draw axis aligned bounding boxes
		        public static final int e_obbBit                                = 0x0010; ///< draw oriented bounding boxes
		        public static final int e_pairBit                               = 0x0020; ///< draw broad-phase pairs
		        public static final int e_centerOfMassBit               = 0x0040; ///< draw center of mass frame
		        public static final int e_controllerBit                 = 0x0080; ///< draw controllers
		From http://code.google.com/p/jbox2d/source/browse/branches/jbox2d-2.0.1/src/org/jbox2d/dynamics/DebugDraw.java
		*/
        map.getAttachedWorld().setDebugDraw(sDD);
        followedPlayer.createPhysic();

        System.out.println("Create and set camera");
        camera = new Camera(followedPlayer.getX(), followedPlayer.getY());
        camera.setTarget(followedPlayer);

        System.out.println("Initialize player position");
        movementMethod(0, 0);

        System.out.println("Init game finished !");
    }

    @Override
    protected void leaveState(GameContainer gc, StateBasedGame sb) throws SlickException {

    }

    @Override
    protected void prepareNifty(Nifty nifty, StateBasedGame sb) {
        java.util.logging.Logger.getAnonymousLogger().getParent().setLevel(java.util.logging.Level.SEVERE);
        java.util.logging.Logger.getLogger("de.lessvoid.nifty.*").setLevel(java.util.logging.Level.SEVERE);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        Screen startScreen = new ScreenBuilder("start") {{
            controller(new GameCoreController());

            layer(new LayerBuilder("MainLayer") {{
                childLayoutAbsolute();
                width("*");
                height("*");

				    /*control(new ConsoleBuilder("console") {{
			    		  width("80%");
			    		  x(percentage(20));
			    		  lines(10);
			    		  alignCenter();
			    		  valignCenter();
			    		  onStartScreenEffect(new EffectBuilder("move") {{
			    		    length(150);
			    		    inherit();
			    		    neverStopRendering(true);
			    		    effectParameter("mode", "in");
			    		    effectParameter("direction", "top");
			    		  }});
				    }});*/

                panel(new PanelBuilder("MainPanel") {{
                    childLayoutAbsolute();
                    x("10px");
                    y(percentage(80));
                    width(percentage(20));
                    height(percentage(18));
                    style("nifty-panel");
                }});
            }});
        }}.build(nifty);

        nifty.addScreen("start", startScreen);
        nifty.gotoScreen("start");
    }

    @Override
    protected void updateGame(GameContainer gc, StateBasedGame sb, int delta) throws SlickException {
        Input input = gc.getInput();
        xMovement = 0;
        yMovement = 0;

        if (input.isKeyDown(Input.KEY_UP)) {
            move = true;
            upMovement = true;
            yMovement = -(movementSpeed * delta);
        } else if (input.isKeyDown(Input.KEY_DOWN)) {
            move = true;
            downMovement = true;
            yMovement = movementSpeed * delta;
        }

        if (input.isKeyDown(Input.KEY_LEFT)) {
            move = true;
            leftMovement = true;
            xMovement = -(movementSpeed * delta);
        } else if (input.isKeyDown(Input.KEY_RIGHT)) {
            move = true;
            rightMovement = true;
            xMovement = movementSpeed * delta;
        }

        if (move) {
            movementMethod(xMovement, yMovement);

            upMovement = false;
            downMovement = false;
            leftMovement = false;
            rightMovement = false;
            move = false;
        }

        map.update();
        followedPlayer.update();
    }

    @Override
    protected void renderGame(GameContainer gc, StateBasedGame sb, Graphics g) throws SlickException {
        camera.render(gc);
        map.render(g);
        map.getAttachedWorld().drawDebugData();
        followedPlayer.render(gc);
        //camera.drawTarget(gc);

        g.drawOval(Math.round(GameCoreState.getOriginWorldX() - GameCoreState.getCamera().getX()) - 10,
                Math.round(GameCoreState.getOriginWorldY() - GameCoreState.getCamera().getY()) - 10, 20, 20);

        g.drawString("Camera X : " + camera.getX(), 10, 50);
        g.drawString("Camera Y : " + camera.getY(), 10, 70);

        g.drawString("OnChunk : " + chunkID, 10, 90);
        g.drawString("Chunk X : " + chunkX, 10, 110);
        g.drawString("Chunk Y : " + chunkY, 10, 130);

        g.drawString("CellID : " + cellID, 10, 150);

        g.drawString("Player X : " + followedPlayer.getX(), 10, 170);
        g.drawString("Player Y : " + followedPlayer.getY(), 10, 190);

        g.drawString("Physics Body : " + map.getAttachedWorld().getBodyCount(), 10, 210);
    }

    @Override
    public int getID() {
        return 5;
    }

    public void movementMethod(float movementX, float movementY) {
        float playerX = followedPlayer.getX();
        float playerY = followedPlayer.getY();

        if (!getWalkability(movementX, movementY, true)) {
            if (!getWalkability(movementX, 0, true)) {
                if (movementX < 0) {
                    leftCollision = true;
                }
                if (movementX > 0) {
                    rightCollision = true;
                }
            }
            if (!getWalkability(0, movementY, true)) {
                if (movementY < 0) {
                    upCollision = true;
                }
                if (movementY > 0) {
                    downCollision = true;
                }
            }
        }

        if ((!leftCollision && movementX < 0) || (!rightCollision && movementX > 0)) {
            followedPlayer.setX(playerX + movementX);
            camera.setX(followedPlayer.getX());
        }

        if ((!upCollision && movementY < 0) || (!downCollision && movementY > 0)) {
            followedPlayer.setY(playerY + movementY);
            camera.setY(followedPlayer.getY());
        }

        leftCollision = false;
        rightCollision = false;
        upCollision = false;
        downCollision = false;
    }

    public boolean getWalkability(float x, float y, boolean preventive) {
        // Get current chunkID
        long _chunkID = map.getChunkIDFromPosition(Math.round(originWorldX - (followedPlayer.getX() + x)),
                Math.round(originWorldY - (followedPlayer.getY() + y)), Map.getChunkWidth(), Map.getChunkHeight());

        if (_chunkID != -1) // If is on a chunk
        {
            // Get position in the chunk
            Chunk chunk = map.getChunkList().get(_chunkID);

            float chunkXWorldScale = chunk.getX() * (Map.getChunkWidth() * Map.getTileWidth());
            float chunkYWorldScale = chunk.getY() * (Map.getChunkHeight() * Map.getTileHeight());

            Point point = CMath.getRelativePointFromWorld(
                    originWorldX + chunkXWorldScale,
                    originWorldY + chunkYWorldScale,
                    followedPlayer.getX() + x,
                    followedPlayer.getY() + y);

            float _chunkX = point.getX();
            float _chunkY = point.getY();

            // Get current cellID
            int _cellID = CMath.getCellIDFromChunk(_chunkX, _chunkY);

            // check if the movement go into a non walkable tile
            int tileX = (int) ((Math.ceil(_chunkX / Map.getTileWidth()) == 0) ? 0 : (Math.ceil(_chunkX / Map.getTileWidth())) - 1);
            int tileY = (int) ((Math.ceil(_chunkY / Map.getTileHeight()) == 0) ? 0 : (Math.ceil(_chunkY / Map.getTileHeight())) - 1);

            if (tileX > Map.getChunkWidth() - 1)
                tileX = Map.getChunkWidth() - 1;
            if (tileY > Map.getChunkHeight() - 1)
                tileY = Map.getChunkHeight() - 1;

            int tileID = chunk.getChunkMap().getTileId(tileX, tileY, 0);

            if (Boolean.parseBoolean(chunk.getChunkMap().getTileProperty(tileID, "walkable", "true"))) {
                if (preventive) {
                    getWalkability(x, y, false);

                    return true;
                } else {
                    chunkID = _chunkID;
                    chunkX = _chunkX;
                    chunkY = _chunkY;
                    cellID = _cellID;
                }
            } else {
                return false;
            }
        }

        return true;
    }

}
