package game.core.states;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.*;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.textfield.builder.TextFieldBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.slick2d.NiftyBasicGameState;
import game.core.controllers.LoginController;
import org.newdawn.slick.state.StateBasedGame;

public class LoginState extends NiftyBasicGameState {

    private static Nifty nifty;
    private static boolean isLogged = false;

    public static Nifty getCurrentNifty() {
        return nifty;
    }

    public static boolean getIsLogged() {
        return isLogged;
    }

    public static void setIsLogged(boolean logged) {
        isLogged = logged;
    }

    @Override
    protected void prepareNifty(Nifty nifty, StateBasedGame sb) {
        LoginState.nifty = nifty;

        java.util.logging.Logger.getAnonymousLogger().getParent().setLevel(java.util.logging.Level.SEVERE);
        java.util.logging.Logger.getLogger("de.lessvoid.nifty.*").setLevel(java.util.logging.Level.SEVERE);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        Screen screen = new ScreenBuilder("start") {{
            controller(new LoginController());

            layer(new LayerBuilder("loginLayer") {{
                backgroundImage("swing/ressources/interfaces/background1.jpg");
                childLayoutCenter();
                width("*");
                height("*");

                panel(new PanelBuilder("identificationPanel") {{
                    childLayoutAbsolute();
                    width(percentage(40));
                    height(percentage(30));
                    style("nifty-panel");

                    control(new LabelBuilder("userLabel", "Username") {{
                        width("200px");
                        height("15px");
                        x("10px");
                        y("30px");
                    }});

                    control(new TextFieldBuilder("userInput", "jones") {{
                        maxLength(20);
                        width("200px");
                        height("20px");
                        x("10px");
                        y("50px");
                    }});

                    control(new LabelBuilder("passLabel", "Password") {{
                        width("200px");
                        height("15px");
                        x("10px");
                        y("80px");
                    }});

                    control(new TextFieldBuilder("passInput", "joness") {{
                        //passwordChar('*');
                        maxLength(20);
                        width("200px");
                        height("20px");
                        x("10px");
                        y("100px");
                    }});

                    control(new ButtonBuilder("loginBut", "Loging in") {{
                        width("100px");
                        height("20px");
                        x("10px");
                        y("140px");
                    }});
                }});
            }});

            new PopupBuilder("popup") {{
                childLayoutCenter();
                backgroundColor("#000a");

                panel(new PanelBuilder("popupPanel") {{
                    childLayoutAbsolute();
                    width("400px");
                    height("150px");
                    style("nifty-panel");

                    text(new TextBuilder("text") {{
                        style("base-font");
                        color("#eeef");
                        width(percentage(100));
                        height(percentage(50));
                        align(Align.Center);
                        valign(VAlign.Center);
                        x("0px");
                        y("0px");
                    }});

                    control(new ButtonBuilder("closePopup", "Close") {{
                        width("100px");
                        height("20px");
                        x((200 - 50) + "px");
                        y("80px");
                    }});
                }});
            }}.registerPopup(LoginState.nifty);
        }}.build(nifty);

        nifty.addScreen("start", screen);
    }

    @Override
    public int getID() {
        return 1;
    }

}
